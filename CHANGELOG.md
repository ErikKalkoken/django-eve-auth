# Change Log

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [1.3.1] - 2023-11-18

## Fixed

- django-esi dependency updated to latest required version after the SSO change

## [1.3.0] - 2023-09-01

## Added

- Added support for Python 3.10 & 3.11
- Added support for Python 4.2

## Changed

- Automatic dark and light mode for Sphinx docs
- Migrated build process to PEP 621
- Removed support for Python 3.7. New minimal Python version is 3.8
- Removed support for Django 2.2 & 4.0

## Fixed

- Logout view does not respect LOGOUT_REDIRECT_URL (#1)

## [1.2.0] - 2022-05-28

## Changed

- Dropped support for Python 3.6 & Django 3.1 since those are now longer maintained
- Added support for Django 4.0

## [1.1.0] - 2021-11-02

## Added

- Show permissions on admin site
- `eve_auth.test_tools.add_permission_to_user_by_name()` to add a permission to a user

### Changed

- `eve_auth.test_tools.create_fake_user()` can now create a user with permissions

## [1.0.0] - 2021-07-15

### Changed

- Updated documentation

## [1.0.0b1] - 2021-06-13

### Added

- Initial beta release
