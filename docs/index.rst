.. django-eve-auth documentation master file, created by
   sphinx-quickstart on Sun Jun 13 14:24:00 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. include:: ../README.md
   :parser: myst_parser.sphinx_

.. toctree::
   :hidden:
   :maxdepth: 3

   operations
   developer
   api
