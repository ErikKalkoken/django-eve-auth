.. currentmodule:: eve_auth

===============
API
===============

This chapter contains the developer reference documentation of the public API for *django-eve-auth*.

Backends
============

.. autoclass:: eve_auth.backends.EveSSOBackend
    :members:

Models
============

.. autoclass:: eve_auth.models.UserEveCharacter
    :members:

.. _api-tools:

Tools
============

.. automodule:: eve_auth.tools.test_tools
    :members:

Views
============

.. automodule:: eve_auth.views
    :members:
